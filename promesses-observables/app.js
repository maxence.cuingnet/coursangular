import { Observable, map, filter } from 'rxjs';

const dataFromPromise = new Promise(item => {
    item(10);
    item(20);
    item(30);
}).then(response => console.log('reponse de ma promesse : ' + response));

const dataFromObservable = new Observable(item => {
    item.next(10);
    item.next(20);
    item.next(30);
    item.next(40);
    item.next(50);
    item.complete();
}).subscribe(response => console.log('reponse de mon observable : ' + response));


dataFromObservable.unsubscribe();

const dataFromObservable2 = new Observable(item => {
    item.next(10);
    item.next(20);
    item.next(30);
    item.next(40);
    item.next(50);
    item.complete();
}).pipe(
    filter(data => data >= 30),
    map(val => val * 2)
);

dataFromObservable2.subscribe(val => console.log(val));
