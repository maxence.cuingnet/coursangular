import { Component, OnInit, inject } from '@angular/core';
import { ProductsService } from '../product-list/products.service';
import { NumericDirective } from '../directives/numeric.directive';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';


@Component({
  selector: 'app-product-create',
  standalone: true,
  imports: [NumericDirective, ReactiveFormsModule],
  templateUrl: './product-create.component.html',
  styleUrl: './product-create.component.css'
})
export class ProductCreateComponent implements OnInit {
  private productService = inject(ProductsService);
  private router = inject(Router);
  private builder = inject(FormBuilder);

  /* productForm = new FormGroup({
    title: new FormControl('', { nonNullable: true }),
    price: new FormControl<number | undefined>(undefined, { nonNullable: true }),
    category: new FormControl('', { nonNullable: true })
  }) */

    productForm: FormGroup<{
      title: FormControl<string>,
      price: FormControl<number | undefined>,
      category: FormControl<string>
    }> | undefined;

  ngOnInit(): void {
      this.buildForm();
  }

  private buildForm() {
    this.productForm = this.builder.nonNullable.group({
      title: ['', [Validators.required, Validators.minLength(3)]],
      price: this.builder.nonNullable.control<number | undefined>(
        undefined,
        [Validators.required, Validators.minLength(3)]
      ),
      category: ['']
    })
  }

  createProduct() {
    this.productService.addProduct(this.productForm!.value).subscribe(
      () => this.router.navigate(['/products'])
    );
  }
}
