import { ResolveFn } from '@angular/router';
import { Product } from './product';
import { ProductsService } from './product-list/products.service';
import { inject } from '@angular/core';


// resolvers : améliore perf app quand les compossants sont :
// 1 - routés
// 2 - demandent une initialisation de données ou complexes

export const productsResolver: ResolveFn<Product[]> = (route) => {
  const productService = inject(ProductsService);
  const limit = Number(route.queryParamMap.get('limit'));
  return productService.getProducts(limit);
};
