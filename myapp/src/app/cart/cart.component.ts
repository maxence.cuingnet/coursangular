import { Component, OnInit, inject } from '@angular/core';
import { FormArray, FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { Product } from '../product';
import { CartService } from './cart.service';
import { ProductsService } from '../product-list/products.service';

@Component({
  selector: 'app-cart',
  standalone: true,
  imports: [ReactiveFormsModule],
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent implements OnInit {
  private cartService = inject(CartService);
  private productsService = inject(ProductsService);

  cartForm = new FormGroup({
    products: new FormArray<FormControl<number>>([])
  })
  products: Product[] = [];

  ngOnInit(): void {
      this.getProducts();
      this.buildForm();
  }

  // recupérer les produits de productService
  // extraire la propriété id pour chaque produit
  // et vérfier si elle existe dans le panier
  // si ok , ajouter au products en local
  private getProducts() {
    this.productsService.getProducts().subscribe(products => {
      this.cartService.cart?.products.forEach( item => {
        const product = products.find(p => p.id === item.productId );
        if (product) {
          this.products.push(product)
        }
      })
      }
      )
  }

  private buildForm() {
    this.products.forEach(() => {
      this.cartForm.controls.products.push(
        new FormControl(1, { nonNullable: true})
      )
    })
  }

}
