import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, OnChanges, inject, OnInit } from '@angular/core';
import { Product } from '../product';
import { AsyncPipe, CurrencyPipe, KeyValuePipe, LowerCasePipe } from '@angular/common';
import { Observable, switchMap } from 'rxjs';
import { ProductsService } from '../product-list/products.service';
import { NumericDirective } from '../directives/numeric.directive';
import { AuthService } from '../auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CartService } from '../cart/cart.service';




@Component({
  selector: 'app-product-detail',
  standalone: true,
  imports: [KeyValuePipe, LowerCasePipe, CurrencyPipe, AsyncPipe, NumericDirective],
  templateUrl: './product-detail.component.html',
  styleUrl: './product-detail.component.css',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductDetailComponent implements OnChanges, OnInit {
  @Input('product') product$: Observable<Product> | undefined;
  @Input() id: string | number | undefined;
  @Output() added = new EventEmitter<Product>();
  @Output() deleted = new EventEmitter();
  public authService = inject(AuthService);
  private router = inject(Router);
  private cartService = inject(CartService);

  constructor(private productService: ProductsService) {}

  ngOnInit(): void {
    /* const id = this.route.snapshot.params['id'];
    this.product$ = this.productService.getProduct(id) */
   /*  this.product$ = this.route.paramMap.pipe(
      switchMap(params => {
        return this.productService.getProduct(Number(params.get('id')));
      })
    ) */
    this.product$ = this.productService.getProduct(Number(this.id!));
  }

  // remarque: n'est finalement pas suivi avec isFirstChange ou firstChange
  ngOnChanges(): void {
      /* const product = changes['product']
      const ancienneValeur = product.previousValue
      const nouvelleValeur = product.currentValue
      if (ancienneValeur !== undefined) {
        console.log('Ancienne Valeur: ', ancienneValeur);
        console.log('Nouvelle Valeur: ', nouvelleValeur);
      } */
     this.id! = Number(this.id)
     this.product$ = this.productService.getProduct(this.id)
  }

  addToCart(id: number) {
    this.cartService.addProduct(id).subscribe();
  }

  changePrice(product: Product, price: string) {
    this.productService.updateProduct(product.id, Number(price)).subscribe(() => {
      this.router.navigate(['/products'])
      }
    );
  }
  /* get productTitle() {
    return this.product?.title
  } */

  remove(product: Product) {
    // appel service
    // emit ==> parent List
    this.productService.deleteProduct(product.id).subscribe(() => {
      this.deleted.emit();
      this.router.navigate(['/products']);
    })
  }
}
