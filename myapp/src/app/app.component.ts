import { Component, Inject, Signal, computed, signal } from '@angular/core';
import { RouterLink, RouterOutlet, RouterLinkActive } from '@angular/router';
import { CopyrightDirective } from './directives/copyright.directive';
import { Observable } from 'rxjs';
import { APP_SETTINGS, AppSettings, appSettings } from './app.settings';
import { AuthComponent } from './auth/auth.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, RouterLink, RouterLinkActive, CopyrightDirective, AuthComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
  providers: [
    { provide: APP_SETTINGS, useValue: appSettings }
  ]
})
export class AppComponent {
  title!: Signal<string>;
  title$ = new Observable(observer => {
    setInterval(() => {
      observer.next();
    }, 2000)
  });
  isLiked = true;
  currentClasses = {
    star: true,
    active: false
  }
  currentStyles = {
    color: 'blue',
    width: '100px'
  }
  currentDate = signal(new Date())

  constructor(@Inject(APP_SETTINGS) public settings: AppSettings) {
    this.title$.subscribe(this.setTitle);
    this.title = computed(() => {
      return `${this.settings.title} (${this.currentDate()})`;
    })
  }

  private setTitle = () => {
    const timestamp = new Date();
    this.currentDate.update(() => timestamp)
  }
}
