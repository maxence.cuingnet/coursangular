import { Component, ViewChild, inject, Signal, signal, OnInit } from '@angular/core';
import { Product } from '../product';
import { ProductsService } from './products.service';
import { AsyncPipe } from '@angular/common';
import { toSignal } from '@angular/core/rxjs-interop';
import { ProductHostDirective } from '../directives/product-host.directive';
import { ActivatedRoute, RouterLink, RouterOutlet } from '@angular/router';
import { Observable, of, switchMap } from 'rxjs';


@Component({
  selector: 'app-product-list',
  standalone: true,
  imports: [RouterLink, RouterOutlet,ProductHostDirective, AsyncPipe],
  templateUrl: './product-list.component.html',
  styleUrl: './product-list.component.css',
  providers: [{
    provide: ProductsService, useClass: ProductsService
  }]
})
export class ProductListComponent implements OnInit {
  // @ViewChild(ProductDetailComponent) productDetail: ProductDetailComponent | undefined;
  // private productSub!: Subscription;
  //products: Signal<Product[]> = signal([]);
  // productService = inject(ProductsService);
  products$: Observable<Product[]> | undefined;
  selectedProduct: Product | undefined;
  // products$ = this.productService.getProducts();
  private route = inject(ActivatedRoute);

  //constructor(private productService: ProductsService) {
    // this.products = toSignal(this.productService.getProducts(), { initialValue: []})
  //}

 ngOnInit(): void {
    // this.products$ = this.route.queryParamMap.pipe(
    //   switchMap(params => this.productService.getProducts(Number(params.get('limit'))))
    // )
    this.products$ = this.route.data.pipe(
      switchMap(data => of(data['products']))
    )
  }

  /* ngOnDestroy() {
    this.productSub?.unsubscribe();
  }
 */
  /* ngAfterViewInit(): void {
      console.log('Product de l\'enfant detail: ', this.productDetail?.product);
  } */

  onAdded(product: Product) {
    alert(`${product.title} est ajouté au panier`);
  }

  private getProducts() {
    // this.productSub = this.productService.getProducts().subscribe(products => this.products = products);
  }

}
