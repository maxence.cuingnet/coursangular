import { Injectable, OnInit, inject } from '@angular/core';
import { Product } from '../product';
import { Observable, switchMap, map, of, retry, tap } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { APP_SETTINGS } from '../app.settings';


@Injectable({
  providedIn: 'root'
})
export class ProductsService  {
  private productsUrl = inject(APP_SETTINGS).apiUrl + '/products';
  private http = inject(HttpClient)
  private products: Product[] = [];


  getProducts(limit?: number): Observable<Product[]> {
    if (this.products.length === 0) {
      const options = {
        params: new HttpParams().set('limit', limit || 10),
        headers: new HttpHeaders({Authorization: 'myToken'})
      }
      return this.http.get<Product[]>(this.productsUrl, options).pipe(
        map(products => {
          this.products = products;
          return this.products
        })
      )
    }
    console.log("Liste produits dans getProducts: ", this.products);
    return of(this.products)
  }

  getProduct(id: number): Observable<Product> {
    // return this.http.get<Product>(`${this.productsUrl}/${id}`);
    console.log("Liste produits dans getProduct: ", this.products);
    const product = this.products.find(p => p.id === id);
    return of(product!);
  }

  addProduct(newProduct: Partial<Product>): Observable<Product> {
    return this.http.post<Product>(this.productsUrl, newProduct).pipe(
      map(product => {
        this.products.push(product);
        return product
      })
    )
  }

  updateProduct(id: number, price: number): Observable<Product> {
    return this.http.patch<Product>(`${this.productsUrl}/${id}`, {
      price
    }).pipe(
      map(product => {
        const index = this.products.findIndex(p => p.id === id);
        this.products[index].price = price
        return product
      })
    )
  }

  deleteProduct(id: number): Observable<void> {
    return this.http.delete<void>(`${this.productsUrl}/${id}`).pipe(
      tap(() => {
        this.products = this.products.filter(p => p.id === id)
      })
    )
  }

}
