import { Component, inject } from '@angular/core';
import { AuthService } from './auth.service';


@Component({
  selector: 'app-auth',
  standalone: true,
  imports: [],
  templateUrl: './auth.component.html',
  styleUrl: './auth.component.css'
})
export class AuthComponent {
  authService = inject(AuthService);

  login() {
    this.authService.login('mor_2314', '83r5^_').subscribe()
  }

  logout() {
    this.authService.logout()
  }
}
