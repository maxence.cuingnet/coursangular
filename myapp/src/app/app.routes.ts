import { Routes } from '@angular/router';
import { ProductListComponent } from './product-list/product-list.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { CartComponent } from './cart/cart.component';
import { ProductCreateComponent } from './product-create/product-create.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductsService } from './product-list/products.service';
import { authGuard } from './auth.guard';
import { checkoutGuard } from './checkout.guard';
import { productsResolver } from './products.resolver';


export const routes: Routes = [
  {
    path: 'products',
    component: ProductListComponent,
    children: [
      { path: 'new', component: ProductCreateComponent },
      { path: ':id', component: ProductDetailComponent },
    ],
    providers: [ProductsService],
    resolve: {
      products: productsResolver
    }
  },
  { path: 'user',
    loadChildren: () => import ('./user/user.routes'),
    canMatch: [authGuard]
  },
  { path: 'cart',
    component: CartComponent,
    // Attention : syntaxe guard différente pour le lazy-loading
    // loadComponent: () => import('./cart/cart.component').then(c => c.CartComponent),
    canActivate: [authGuard],
    canDeactivate: [checkoutGuard]
  },
  { path: '', redirectTo: 'products', pathMatch: 'full'},
  { path: '**', component: PagenotfoundComponent }
];
